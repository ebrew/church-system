from flask_jwt_extended import JWTManager
from app import app, api, db
from app.resources import (MemberResources, MemberResourcesUserID, AssetResources,
                           DepartmentResources, PaymentResources, AccountResources,AccountResourcesLGA,
                           AssetResourcesGetAsset, DepartmentResourcesGetDeptID, MemberResourcesGetDeptMem)


@app.before_first_request
def create_table():
    db.create_all()


jwt = JWTManager(app)

# Endpoints
api.add_resource(MemberResources, '/member')  # Add a member
# Search a member by id, first name or last name
api.add_resource(MemberResourcesUserID, '/member/<member_id>')
api.add_resource(DepartmentResources, '/department') # Add a department
# Search a department by its id or name
api.add_resource(DepartmentResourcesGetDeptID, '/department/<department_id>')
# Search department members
api.add_resource(MemberResourcesGetDeptMem, '/member/<department_id>')
api.add_resource(AssetResources, '/asset') # Add an asset
api.add_resource(AssetResourcesGetAsset, '/asset/<asset_name>')
# Make payment
api.add_resource(PaymentResources, '/payment')
api.add_resource(AccountResources, '/account')  # Login endpoints
api.add_resource(AccountResourcesLGA, '/account')  # Add login account




if __name__ == "__main__":
    app.run(port=5000, debug=True)
