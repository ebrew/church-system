from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship, backref
from app import db  # app has entry point __init__
from sqlalchemy.ext.declarative import declarative_base
from passlib.hash import pbkdf2_sha256 as sha256

# Model the database(sqlalchemy makes it easier)
Base = declarative_base()

# Comment


class BaseModel(db.Model):
    __abstract__ = True

    def save(self):
        db.session.add(self)
        db.session.commit()


class MemberModel(BaseModel):
    __tablename__ = 'member'

    member_id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(5))
    f_name = db.Column(db.String(45))
    l_name = db.Column(db.String(45))
    gender = db.Column(db.String(4))
    date_of_birth = db.Column(db.String(10))
    date_of_registration = db.Column(db.String(10))
    contact = db.Column(db.String(13))
    email = db.Column(db.String(45))
    residential_address = db.Column(db.String(45))
    city = db.Column(db.String(20))
    country = db.Column(db.String(20))
    occupation = db.Column(db.String(20))
    marital_status = db.Column(db.String(10))
    number_of_children = db.Column(db.String(10))
    department_id = db.Column(db.Integer, ForeignKey('department.department_id'))
    admin = db.Column(db.String(4))
    executive = db.Column(db.String(45), nullable=True)
    account = relationship("AccountModel", uselist=False, back_populates="member")
    payments = relationship("PaymentModel", backref=backref("member"))
    department = relationship("DepartmentModel", back_populates="members")

    def convert_to_json(self):  # Return python dictionary to be sent in json form in the frontend.
        # Json is just a string or text sent from one app to another app.
        member = {
            'member_id': self.member_id,
            'title': self.title,
            'f_name': self.f_name,
            'l_name': self.l_name,
            'gender': self.gender,
            'date_of_birth': self.date_of_birth,
            # 'date_of_registration': self.date_of_registration,
            'contact': self.contact,
            'email': self.email,
            'residential_address': self.residential_address,
            'city': self.city,
            'country': self.country,
            'occupation': self.occupation,
            'marital_status': self.marital_status,
            'number_of_children': self.number_of_children,
            'department_id': self.department_id,
            'admin': self.admin,
            'executive': self.executive
        }
        return member

    # '@classmethod' Shows that it doesn't need 'self' to be passed(this pointer for c++)
    # Queries to search for a user entered identity
    @classmethod
    def find_by_f_name(cls, f_name):
        return cls.query.filter_by(user_name=f_name)

    @classmethod
    def find_by_l_name(cls, l_name):
        return cls.query.filter_by(user_name=l_name)

    @classmethod
    def find_by_id(cls, member_id):
        return cls.query.filter_by(member_id=member_id)

    @classmethod
    def find_by_deptmem(cls, department_id):
        return cls.query.filter_by(department_id=department_id)


class DepartmentModel(BaseModel):
    __tablename__ = 'department'

    department_id = db.Column(db.Integer, primary_key=True)
    department_name = db.Column(db.String(45))
    members = relationship("MemberModel", back_populates="department")

    def convert_to_json(self):
        department = {
            'department_id': self.department_id,
            'department_name': self.department_name
        }
        return department

    @classmethod
    def find_by_dept(cls, department_name):
        return cls.query.filter_by(department_name=department_name)

    @classmethod
    def find_by_deptid(cls, department_id):
        return cls.query.filter_by(department_id=department_id)


class AssetModel(BaseModel):
    __tablename__ = 'asset'

    asset_id = db.Column(db.Integer, primary_key=True)
    asset_name = db.Column(db.String(45))
    asset_date_of_registration = db.Column(db.String(10))
    asset_proof = db.Column(db.String(45))

    def convert_to_json(self):
        asset = {
            'asset_id': self.asset_id,
            'asset_name': self.asset_name,
            'asset_date_of_registration': self.asset_date_of_registration,
            'asset_proof': self.asset_proof
        }
        return asset

    @classmethod
    def find_by_name(cls, asset_name):
        return cls.query.filter_by(asset_name=asset_name)


class PaymentModel(BaseModel):
    __tablename__ = 'payment'

    payment_id = db.Column(db.Integer, primary_key=True)
    payment_type = db.Column(db.String(5))
    payment_date = db.Column(db.String(10))
    amount = db.Column(db.Float(precision=2))
    member_id = db.Column(db.Integer, ForeignKey('member.member_id'))

    def convert_to_json(self):
        payment = {
            'payment_id': self.payment_id,
            'payment_type': self.payment_type,
            # 'payment_date': self.payment_date,
            'amount': self.amount,
            'member_id': self.member_id
        }
        return payment


class AccountModel(BaseModel):
    __tablename__ = 'account'

    id = db.Column(db.Integer, primary_key=True)
    user_name = db.Column(db.String(15))
    user_password = db.Column(db.String(100))
    member_id = db.Column(db.Integer, ForeignKey('member.member_id'))
    member = relationship("MemberModel", back_populates="account")

    def convert_to_json(self):
        account = {
            'user_name': self.user_name,
            'user_password': self.user_password,
            'member_id': self.member_id
        }

    @classmethod
    def find_by_name(cls, user_name):
        return cls.query.filter_by(user_name=user_name)

    @staticmethod
    def generate_hash(user_password):
        return sha256.hash(user_password)

    @staticmethod
    def verify_hash(user_password, hash):
        return sha256.verify(user_password, hash)
