from flask_restful import reqparse
# from datetime import datetime

# The parser is responsible for validation of data type passed to the database.
# Parser for a new member details
memberParser = reqparse.RequestParser()
memberParser.add_argument(
    'member_id',
    type=int,
    required=False,
    help="This field is set to default." 
)

memberParser.add_argument(
    'title',
    type=str,
    required=True,
    help="This field is optional." 
)

memberParser.add_argument(
    'f_name',
    type=str,
    required=True,
    help="This field cannot be blank." 
)

memberParser.add_argument(
    'l_name',
    type=str,
    required=True,
    help="This field cannot be blank."
)

memberParser.add_argument(
    'gender',
    type=str,
    required=True,
    help="This field cannot be blank." 
)

memberParser.add_argument(
    'date_of_birth',
    type=str,
    required=True,
    help="This field cannot be blank." 
)

memberParser.add_argument(
    'date_of_registration',
    type=str,
    required=True,
    help="This field cannot be blank."
)

memberParser.add_argument(
    'contact',
    type=str,
    required=True,
    help="This field starts with the country code +233 if you a Ghanaian."
)

memberParser.add_argument(
    'email',
    type=str,
    required=True,
    help="This field is optional." 
)

memberParser.add_argument(
    'residential_address',
    type=str,
    required=True,
    help="This field cannot be blank."
)

memberParser.add_argument(
    'city',
    type=str,
    required=True,
    help="This field cannot be blank."
)

memberParser.add_argument(
    'country',
    type=str,
    required=True,
    help="This field cannot be blank."
)

memberParser.add_argument(
    'occupation',
    type=str,
    required=True,
    help="This field cannot be blank."
)

memberParser.add_argument(
    'marital_status',
    type=str,
    required=True,
    help="This field cannot be blank."
)

memberParser.add_argument(
    'number_of_children',
    type=str,
    required=True,
    help="This field may be left blank."
)

memberParser.add_argument(
    'department_id',
    type=int,
    required=True,
    help="This field cannot be blank."
)

memberParser.add_argument(
    'admin',
    type=str,
    required=True,
    help="This field cannot be blank."
)


# Parser for a new department details
departmentParser = reqparse.RequestParser()
departmentParser.add_argument(
    'department_id',
    type=int,
    required=False,
    help="This field is set to default"    
)


departmentParser.add_argument(
    'department_name',
    type=str,
    required=True,
    help="This field cannot be blank."    
)


assetParser = reqparse.RequestParser()
assetParser.add_argument(
    'asset_id',
    type=int,
    required=False,
    help="This field is set to default"    
)

assetParser.add_argument(
    'asset_name',
    type=str,
    required=True,
    help="This field cannot be blank."    
)

assetParser.add_argument(
    'asset_date_of_registration',
    type=str,
    required=True,
    help="This field cannot be blank."    
)

assetParser.add_argument(
    'asset_proof',
    type=str,
    required=True,
    help="This field requires the right quote from the asset document."
)

paymentParser = reqparse.RequestParser()
paymentParser.add_argument(
    'payment_id',
    type=int,
    required=False,
    help="This field is set to default"    
)

paymentParser.add_argument(
    'payment_type',
    type=str,
    required=True,
    help="This field cannot be blank."    
)

paymentParser.add_argument(
    'payment_date',
    type=str,
    required=True,
    help="This field is required"
)

paymentParser.add_argument(
    'amount',
    type=float,
    required=True,
    help="This field cannot be blank."    
)

paymentParser.add_argument(
    'member_id',
    type=int,
    required=True,
    help="This field cannot be blank."    
)

accountParser = reqparse.RequestParser()
accountParser.add_argument(
    'user_name',
    type=str,
    required=True,
    help="This field cannot be blank."
)

accountParser.add_argument(
    'user_password',
    type=str,
    required=True,
    help="This field cannot be blank."
)

accountParser.add_argument(
    'id',
    type=int,
    required=False,
    help="This field is set to default."
)

accountParser.add_argument(
    'member_id',
    type=int,
    required=True,
    help="This field cannot be left blank."
)
