# These have to do with the verbs associated with the http

# This converts python dictionary to json.
from flask_restful import Resource, reqparse
from .models import (MemberModel, DepartmentModel,
                     PaymentModel, AssetModel, AccountModel)
from .parser import (memberParser, departmentParser, paymentParser, assetParser,accountParser)
from . import constants  # The constants to import are in the same folder.
# The below import is for Login aspects...importing from the library
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token
)

# Contains all the http verbs regarding member model


class MemberResources(Resource):
    @staticmethod
    def post():  # New member to be added
        data = memberParser.parse_args(strict=True)
        # A variable to hold the new data for parser to verify.

        member = MemberModel()  # To be passed to the member model for the database
        member.title = data['title']
        member.f_name = data['f_name']
        member.l_name = data['l_name']
        member.date_of_birth = data['date_of_birth']
        member.date_of_registration = data['date_of_registration']
        member.contact = data['contact']
        member.email = data['email']
        member.residential_address = data['residential_address']
        member.city = data['city']
        member.country = data['country']
        member.occupation = data['occupation']
        member.marital_status = data['marital_status']
        member.number_of_children = data['number_of_children']
        member.department_id = data['department_id']
        member.admin = data['admin']
        member.save()  # Calling  a save function to save the new member data
        return member.convert_to_json(), constants.HTTP_201_CREATED
        # Returning the saved data for a proof.


# class created for the second get since the previous one can't contain two get
class MemberResourcesUserID(Resource):
    @staticmethod
    def get(member_id):
        try:
            var = int(member_id)
            members = MemberModel.find_by_id(member_id)
            print('Im running')
            in_json = []
            for i in members:
                in_json.append(i.convert_to_json())
            if len(in_json) == 0:
                return {"error": "Member not found"}
            return in_json, constants.HTTP_200_OK
        except TypeError:
                members = MemberModel.find_by_f_name(member_id)
                print("I'm running")
                in_json = []
                for i in members:
                    in_json.append(i.convert_to_json())
                if len(in_json) == 0:
                    members = MemberModel.find_by_l_name(member_id)
                    in_json = []
                    for i in members:
                        in_json.append(i.convert_to_json())
                    if len(in_json) == 0:
                        return {"error": "Member not found"}
                in_json = []  # Convert the raw data to list
                for i in members:
                    in_json.append(i.convert_to_json())
                return in_json, constants.HTTP_200_OK


class MemberResourcesGetDeptMem(Resource):
    @staticmethod
    def get(department_id):
        members = MemberModel.find_by_deptmem(department_id)
        in_json = []  # Convert the raw data to list
        for i in members:
            in_json.append(i.convert_to_json())
        if len(in_json) == 0:
            return {"error": "Member not found for the entered department"}
        return in_json, constants.HTTP_200_OK


class DepartmentResources(Resource):
    @staticmethod
    def post():
        data = departmentParser.parse_args(strict=True)

        department = DepartmentModel()
        department.department_name = data['department_name']
        department.save()
        return department.convert_to_json(), constants.HTTP_201_CREATED


class DepartmentResourcesGetDept(Resource):
    @staticmethod
    def get(department_name):
        departments = DepartmentModel.find_by_dept(department_name)
        in_json = []
        for i in departments:
            in_json.append(i.convert_to_json())
        if len(in_json) == 0:
            return {"error": "Department not found"}
        return in_json, constants.HTTP_200_OK


class DepartmentResourcesGetDeptID(Resource):
    @staticmethod
    def get(department_id):
        try:
            val = int(department_id)
            departments = DepartmentModel.find_by_deptid(department_id)
            print('Im running')
            in_json = []
            for i in departments:
                in_json.append(i.convert_to_json())
            if len(in_json) == 0:
                return {"error": "Department not found"}
            in_json = []
            for i in departments:
                in_json.append(i.convert_to_json())
            return in_json, constants.HTTP_200_OK
        except TypeError:
            departments = DepartmentModel.find_by_dept(department_id)
            print('Im running')
            in_json = []
            for i in departments:
                in_json.append(i.convert_to_json())
            if len(in_json) == 0:
                return {"error": "Department not found"}
            in_json = []
            for i in departments:
                in_json.append(i.convert_to_json())
            return in_json, constants.HTTP_200_OK


class AssetResources(Resource):
    @staticmethod
    def post():
        data = assetParser.parse_args(strict=True)

        asset = AssetModel()
        asset.asset_name = data['asset_name']
        asset.asset_date_of_registration = data['asset_date_of_registration']
        asset.asset_proof = data['asset_proof']
        asset.save()
        return asset.convert_to_json(), constants.HTTP_201_CREATED


class AssetResourcesGetAsset(Resource):
    @staticmethod
    def get(asset_name):
        assets = AssetModel.find_by_name(asset_name)
        in_json = []
        for i in assets:
            in_json.append(i.convert_to_json())
        if len(in_json) == 0:
            return {"error": "Asset name not found"}
        return in_json, constants.HTTP_200_OK


class PaymentResources(Resource):
    @staticmethod
    def post():
        data = paymentParser.parse_args(strict=True)

        payment = PaymentModel()
        payment.payment_type = data['payment_type']
        payment.payment_date = data['payment_date']
        payment.amount = data['amount']
        payment.member_id = data['member_id']
        payment.save()
        return payment.convert_to_json(), constants.HTTP_201_CREATED


class AccountResourcesLGA(Resource):
    @staticmethod
    def post():  # Login
        data = accountParser.parse_args(strict=True)

        current_member = AccountModel.find_by_name(data['user_name'])

        in_json = []
        for i in current_member:
            in_json.append(i.convert_to_json())
        if len(in_json) == 0:
            return {'message': "Member {} doesn't exist".format(data['user_name'])}

        if AccountModel.verify_hash(data['user_password'], current_member.user_password):
            access_token = create_access_token(identity=data['user_name'])
            refresh_token = create_refresh_token(identity=data['user_name'])
            return {
                'message': 'Logged in as {}'.format(current_member.user_name),
                'access_token': access_token,
                'refresh_token': refresh_token
            }
        else:
            return {'message': 'Invalid credentials'}


class AccountResources(Resource):
    @staticmethod
    def post():
        data = accountParser.parse_args(strict=True)

        account = AccountModel()
        account.member_id = data['member_id']
        account.user_name = data['user_name']
        account.user_password = AccountModel.generate_hash(data['user_password'])
        account.save()

#  Would work on it
# class MemberLogout(Resource):
#     @jwt_refresh_token_required
#     def post(self):

