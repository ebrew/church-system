import React from 'react';
import '../images/LFC_logo.png';
import DrawerToggleButton from '../SideDrawer/DrawerToggleButton';
import './Toolbar.css';
import { NavLink } from 'react-router-dom';


const toolbar = props=>(
    <header className="toolbar">
        <nav className="toolbar_navigation">
            <div className="toolbar_toggle-button">
                <DrawerToggleButton click={props.drawerClickHandler} />
            </div>
            <div className="toolbar_logo"> <a href="/"> <img src="LFC_logo.png" alt="Church Logo"/>  </a> </div>
            <div className="spacer"/>
            <div className="toolbar_navigation-items">
            
                <ul>
                    <li>
                        <NavLink to="/HomePage" exact activeStyle={{color:'violet'}} >Home</NavLink>
                    </li>
                    <li>
                        <NavLink to="/Devotional" exact activeStyle={{color:'violet'}} >Devotional</NavLink>
                    </li>
                    <li>
                        <NavLink to="/GivingOnline" exact activeStyle={{color:'violet'}}>Give Online</NavLink>
                    </li>
                    <li>
                        <NavLink to="/UserProfile" exact activeStyle={{color:'violet'}}>User Profile</NavLink>
                    </li>
                    <li> 
                        <NavLink to = "/Messaages" exact activeStyle={{color:'violet'}}> Messages</NavLink> 
                    </li>
                    <li> 
                        <NavLink to = "/MainPage" exact activeStyle={{color:'violet'}}> Log Out </NavLink> 
                    </li>
                </ul>
            </div>
        </nav>
    </header>
);


export default toolbar;