import React, { Component } from "react";
import './Form.css';
import {object} from 'prop-types';
import UserAccount from "../UserAccount/UserAccount";



const emailRegex = RegExp(/*[a-zA-Z0-9] */);


const formValid = formErrors =>{
    let valid =true;

    object.values(formErrors).forEach(val => {
        val.length > 0 && (valid = false);
    });

    return valid;
};
class Form extends Component {

    constructor(props) {
      super(props)
    
      this.state = {
            firstName: null,
            //firstNameError: "",
            lastName: null,
           // lastNameError:"",
            email: null,
            //emailError:"",
            password: null,
            //passwordError:"",
            confirmPassword: null,
           // confirmPasswordError: ""
           formErrors: {
               firstName: "",
               lastName: "",
               email: "",
               password: "",
               confirmPassword: ""
            }
        }
        this.handleChange = this.handleChange.bind(this)

    }
    
 
      /*  change = event => {
            //this.props.onChange({ [event.target.name]: event.target.value });
            this.setState({
               [event.target.name]: event.target.value
           });
       }; */

        /* validate = () =>{
            let isError = false;
            const errorMessage = {};

            if (this.state.firstName < 2){
                isError = true;
                errorMessage.firstName = "Invalid name";
            }

            if (isError){
                this.setState({
                    ...this.state,
                    ...errorMessage
                });
            }
            return isError;
        };  */

       /* onSubmit = event => {
           event.preventDefault();
          // alert (`${[event.target.name]}`)

           // this.props.onSubmit(this.state);
           
           const error = this.validate();

           //  const err = this.validate();
            if (!error)
            { // clear form
            this.setState({
               firstName: "",
               lastName: "",
               email: "",
               password: "",
               confirmPassword: ""
            });
            this.props.onChange({
               firstName: "",
               lastName: "",
               email: "",
               password: "",
               confirmPassword: ""
            });
            }
       };  */
   
 
       handleSubmit = e => {
           e.preventDefault ();

           if (formValid(this.state.formErrors)) {
               console.log (`
                First Name: ${this.state.firstName}
                Last Name: ${this.state.lastName}
                Email: ${this.state.email}
                Password: ${this.state.password}
               `)
           } else {
               console.error ('Form invalid');
           }
       };

       handleChange = e => {
           e.preventDefault();
           const { name, value} = e.target;
           let formErrors = this.state.formErrors;

           switch (name){
               case 'firstName':
                    formErrors.firstName = value.length < 6 && value.length > 0 
                    ? 'minimum 3 Characters': "";
                break;
               case 'lastName':
                    formErrors.lastName = value.length < 6 && value.length > 0 
                    ? 'minimum 3 Characters': "";
                break;
               case 'email':
                    formErrors.email = 
                        emailRegex.test(value) && value.length > 0 
                        ? '': " Invalid email";
                break;
               case 'password':
                    formErrors.password = value.length < 6 && value.length > 0 
                    ? 'minimum 3 Characters': " ";
                break;
                default:
                break; 
           }

           this.setState({formErrors,[name]: value }, ()=> console.log(this.state))
       }
    render() {

        return ( 
            <div className = "wrapper" >
                <div className = "form-wrapper" >
                    <h1> Create Account </h1> 
                        <form onSubmit = {this.handleSubmit} noValidate >
                            <div className = "firstName" >
                                <label htmlFor = "firstName" > First Name </label> 
                                    <input type = "text"
                                        className = " "
                                        placeholder = "First Name"
                                        name = "firstName"
                                        value = {
                                            this.state.firstName
                                        }
                                        onChange = {
                                            this.handleChange
                                        }
                                        errorText = {
                                            this.state.firstNameError
                                        }
                                    /> 
                                   
                            </div> 
                            <div className = "lastName" >
                                <label htmlFor = "lastName" > Last Name </label> 
                                    <input type = "text"
                                        className = ""
                                        placeholder = "Last Name"
                                        name = "lastName"
                                        value = {
                                           this.state.lastName
                                        }
                                        onChange = {
                                           this.change
                                       }
                                       errorText = {
                                           this.state.lastNameError
                                       }
                                    /> 
                            </div> 
                            <div className = "email" >
                                <label htmlFor = "email" > Email </label> 
                                    <input className = ""
                                            type = "email"
                                            placeholder = "Email"
                                            name = "email"
                                            value = {
                                                this.state.email
                                            }
                                            onChange = {
                                                e => this.handleChange
                                            }
                                            errorText = {
                                                this.state.emailError
                                            }
                                    /> 
                            </div> 
                            <div className = "password" >
                                <label htmlFor = "password" > Password </label> 
                                    <input className = " "
                                            type = "password"
                                            placeholder = "Password"
                                            name = "password"
                                            value = {
                                                this.state.password
                                            }
                                            onChange = {
                                                this.handleChange
                                            }
                                            errorText = {
                                                this.state.passwordError
                                            }
                                    /> 
                            </div>
                            < div className = "confirmPassword" >
                                <label htmlFor = "confirmPassword" > Confirm Password </label>  
                                    <input className = " "
                                        type = "password"
                                        placeholder = "Confirm Password"
                                        name = "confirmPassword"
                                       value = {
                                           this.state.confirmPassword
                                       }
                                       onChange = {
                                           this.handleChange
                                       }
                                       errorText = {
                                           this.state.confirmPasswordError
                                       }
                                    />  
                            </div>                             
                            <div className = "createAccount" >
                                <button type = "submit" > Create Account </button> 
                                    <small > Already Have an Account ? </small>
                                    <small>Sign In</small> 
                            </div> 
                        </form> 
                </div> 
            </div>
        );
    };
}




export default Form;
