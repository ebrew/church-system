import React, {
    Component
} from 'react';
import Form from './components/Form/Form';
import UserAccount from './components/UserAccount/UserAccount.js';
import AdminHeaderbar from './components/adminpage/AdminHeaderbar';
//import SmallGroup from './components/adminpage/SmallGroup'


class App extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isLoggedIn: false
        }
        this.handleClick = this.handleClick.bind(this)
    }

    handleClick() {
        this.setState(prevState => {
            return {
                isLoggedIn: !prevState.isLoggedIn
            }
        })
    }
    render() {
        let buttonText = this.state.isLoggedIn ? "LOG OUT" : "LOG IN"

        return ( 
            <div className = "App" >
                <button onClick = {
                    this.handleClick
                    } > 
                        {buttonText} 
                </button> 
            </div>
        )
    }
}


export default App;
