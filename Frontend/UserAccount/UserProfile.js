import React, { Component } from 'react'

class UserProfile extends Component {

  constructor(props) {
    super(props);
    this.state = {
      list: [
        "go",
        "come",
        "learn"
      ]
    }
  }

//addItem function
addItem(e){
  //Prevent button click form submitting form
  e.preventDefault();
  
  //create variables for the list, the item to add, and the form
  let list = this.state.list ;
  const newItem=document.getElementById("addInput");
  const form = document.getElementById("addItemForm");


  //if the input has a value
  if (newItem.value != ""){
    //add the new item to the end of the array
    list.push(newItem.value);
    //set the state for list
    this.setState({
      list: list
    });

    //form is reset
    newItem.classList.remove("is-danger");
    form.reset();
  } else {
    //make border red without input
    newItem.classList.add("is-danger");
  }
  this.addItem = this.addItem.bind(this);
}

//remove function
/* removeItem(item) {
  // Put our list into an array
  const list = this.state.list.slice();
  // Check to see if item passed in matches item in array
  list.some((el, i) => {
    if (el === item) {
      // If item matches, remove it from array
      list.splice(i, 1);
      return true;
    }
  });
  // Set state to list
  this.setState({
    list: list
  });
  this.removeItem = this.removeItem.bind(this);
} */




  render() {
    return (
      <div className="content">
        <div className="container">
          <section className="section">
            <ul>
              {this.state.list.map(item => (
                <li key={item}>{item}</li>
              ))}
            </ul>
          </section>
          <hr/>
          <section className="section">
            <form className="form" id="addItemForm">
                <input 
                  type="text"
                  className="input"
                  id="addInput"
                  placeholder="additional information..."
                  />
                  <button className="addInfoButton" onClick={this.addItem}>
                    Add Info  
                  </button>
            </form>
          </section>
          
        </div>
      </div>
    )
  }
}

export default UserProfile


/* <ul>
            {this.state.list.map(item => (
              <li key={item}>
                {item} &nbsp;
                <span
                  className="delete"
                  onClick={() => this.removeItem(item)}
                  />
              </li>
            ))}
          </ul> */