import React, { Component } from 'react'
import {BrowserRouter, Route, Switch} from 'react-router-dom';

import Toolbar from '../Toolbar/Toolbar.js';
import SideDrawer from '../SideDrawer/SideDrawer.js';
import Backdrop from '../Backdrop/Backdrop.js';
import HomePage from './HomePage.js';
import Devotional from './Devotional.js';
import GivingOnline from './GivingOnline.js';
import WebPage from '../mainpage/WebPage';




export class UserAccount extends Component {
   state = {
     sideDrawerOpen: false
   };
   drawerToggleClickHandler = () => {
     this.setState((prevState) => {
       return {
         sideDrawerOpen: !prevState.sideDrawerOpen
       };
     });
   };

   backdropClickHandler = () => {
     this.setState({
       sideDrawerOpen: false
     });
   };


    render() {
      let backdrop;

      if (this.state.sideDrawerOpen) {
       backdrop = <Backdrop click = {this.backdropClickHandler}/>
      }

      return (
        <BrowserRouter>
        <div style = {{height: '100%'}} >
          <Toolbar drawerClickHandler = {this.drawerToggleClickHandler}/> 
          <SideDrawer show = {this.state.sideDrawerOpen}/> 
          {backdrop} 
          <main style = {{marginTop: '64px'}} >
            <p> This is the page content </p>  
          </main>
          <Switch>
            <Route path="/HomePage" component={HomePage}/>
            <Route path="/Devotional" component={Devotional}/>
            <Route path="/GivingOnline" component={GivingOnline}/>
            <Route path="/MainPage" component={WebPage}/>
          </Switch> 
        </div>
        </BrowserRouter>
     );
   }
}

export default UserAccount
