import React from 'react';
import './SideDrawer.css';
import '../images/img4.jpg';

import {NavLink} from 'react-router-dom';

const sideDrawer = props => {

    let drawerClasses ='side-drawer';
    if (props.show) {
        drawerClasses ='side-drawer open';
    }
    return(
    <nav className={drawerClasses}>
    <img src="img4"/>
                <ul>
                    <li>
                        <NavLink to="/HomePage" exact activeStyle={{color:'violet'}} >Home</NavLink>
                    </li>
                    <li>
                        <NavLink to="/Devotional" exact activeStyle={{color:'violet'}} >Devotional</NavLink>
                    </li>
                    <li>
                        <NavLink to="/GivingOnline" exact activeStyle={{color:'violet'}}>Give Online</NavLink>
                    </li>
                    <li>
                        <NavLink to="/UserProfile" exact activeStyle={{color:'violet'}}>User Profile</NavLink>
                    </li>
                    <li> 
                        <NavLink to = "/Messaages" exact activeStyle={{color:'violet'}}> Messages</NavLink> 
                    </li>
                    <li> 
                        <NavLink to = "/MainPage" exact activeStyle={{color:'violet'}}> Log Out </NavLink> 
                    </li>
                </ul>
    </nav>
    );
};


export default sideDrawer ;